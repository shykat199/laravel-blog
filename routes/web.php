<?php

use App\Http\Controllers\admin\auth\AdminBlogController;
use App\Http\Controllers\admin\auth\DashboardController;
use App\Http\Controllers\admin\auth\UserLogInController;
use App\Http\Controllers\admin\auth\UserRegisterController;
use App\Http\Controllers\admin\PostController;
use Illuminate\Support\Facades\Route;


require base_path('routes/blog.php');


Route::prefix('role')->group(function () {

    Route::get('/login', [UserLogInController::class, 'index'])->name('admin.login');
    Route::post('/user/login', [UserLogInController::class, 'login'])->name('admin.user.login');
    Route::get('/user/logout', [UserLogInController::class, 'logout'])->name('admin.logout');
    Route::get('/register', [UserRegisterController::class, 'index'])->name('admin.register');
    Route::post('/store_user', [UserRegisterController::class, 'store'])->name('admin.store.new');



    Route::middleware('auth')->group(function () {

        Route::get('/dashboard', [AdminBlogController::class, 'index'])->name("admin.dashboard");

        Route::middleware('blogger:blogger,admin,manager')->group(function (){

            Route::get('/index_blog', [PostController::class, 'index'])->name('blog.index');
            Route::get('/blog_create', [PostController::class, 'create'])->name('blog.create');
            Route::post('/blog_store', [PostController::class, 'store'])->name('blog.store');
            Route::get('/blog_show/{id}', [PostController::class, 'show'])->name('blog.show');
            Route::get('/blog_update/{id}', [PostController::class, 'edit'])->name('blog.edit');
            Route::post('/blog_update/{id}', [PostController::class, 'update'])->name('blog.update');
            Route::get('/blog_delete/{id}', [PostController::class, 'destroy'])->name('blog.delete');
            Route::get('/blog_inactive', [PostController::class, 'inactive'])->name('blog.inactive');
            Route::get('/show_inactive_blog/{id}', [PostController::class, 'showInactive'])->name('blog.inactive.show');
            Route::get('/edit_inactive_blog/{id}', [PostController::class, 'editInactive'])->name('blog.inactive.edit');
            Route::get('/update_inactive_blog/{id}', [PostController::class, 'updateInactive'])->name('blog.inactive.update');
            Route::post('/blog_active', [PostController::class, 'blogActive'])->name('blog.inactive.active');
            Route::post('/blog_inactive', [PostController::class, 'blogInActive'])->name('blog.active.inactive');
        });

        require base_path('routes/category.php');
        require base_path('routes/tag.php');
        require base_path('routes/user.php');

    });


});
