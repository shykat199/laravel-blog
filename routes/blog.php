<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\frontend\BlogController;
use App\Http\Controllers\frontend\BlogDetailsController;

Route::prefix('/blog')->group(function (){
    Route::get('/index',[BlogController::class,'index'])->name('blog.index');
    Route::get('/single/blog/details',[BlogDetailsController::class,'index'])->name('blog.single.index');
    Route::get('/single/blog/details/{id}',[BlogDetailsController::class,'show'])->name('blog.single.show');
});
