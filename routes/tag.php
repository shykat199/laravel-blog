<?php

use App\Http\Controllers\admin\TagController;
use Illuminate\Support\Facades\Route;


Route::prefix('tag')->middleware('blogger:admin,manager')->group(function (){

    Route::get('/index',[TagController::class,'index'])->name('tag.index');
//  Route::get('/create',[CategoryController::class,'create'])->name('category.create');
    Route::post('/store',[TagController::class,'store'])->name('tag.store');
    Route::post('/update',[TagController::class,'update'])->name('tag.update');
    Route::get('/delete/{id}',[TagController::class,'destroy'])->name('tag.delete');

});



