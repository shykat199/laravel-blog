<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class BloggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next, string $roles): Response
    {
        //dd($role);
        //$found = false;
        foreach ($roles as $role) {
            try {
                if (Auth::user()->user_role === $role) {

                    return $next($request);

                }
            } catch (ModelNotFoundException $exception) {
                dd('Could not find role ' . $role);
            }
        }
        Auth::logout();
        return redirect()->route('admin.login')->with('error', 'Please Login First');

    }
}
