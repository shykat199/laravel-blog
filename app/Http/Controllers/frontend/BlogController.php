<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
//        $allCategories = Category::all();
        //$searchName = $request->input('search');

        $allCategories = Category::leftJoin('posts', 'posts.category_id', 'categories.id')
            ->where('posts.post_status',1)
            ->select('categories.*', DB::raw('count(posts.id) as total'))
            ->groupBy('categories.id')->get();

        // $allCategories=$allCategories->paginate(5);
         //dd($allCategories);
        //return $allCategories;

        //$allPosts = Post::with("categories")->where('post_status', '=', 1)->get();
        $allPosts = Post::query()->with("categories")->where('post_status', '=', 1);

        if ($request->has('search')) {
            $allPosts = $allPosts->where('post_title', 'like', "%{$request->get('search')}%")
                ->orWhere('post_description', 'like', "%{$request->get('search')}%");
        }
        //dd($allPosts);
        $allPosts = $allPosts->get();

        $posts = Post::where('post_status', 1)->inRandomOrder()->limit(4)->with(['tags', 'categories', 'users'])->get();

        return view('blog.index', compact('allCategories', 'allPosts', 'posts'));
    }

}
