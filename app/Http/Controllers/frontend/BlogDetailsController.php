<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogDetailsController extends Controller
{
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $allCategories = Category::leftJoin('posts', 'posts.category_id', 'categories.id')
            ->where('posts.post_status',1)
            ->select('categories.*', DB::raw('count(posts.id) as total'))
            ->groupBy('categories.id')->get();
        return view('blog.blog_detail');
    }

    public function show(string $id)
    {
        $data['blogDetails'] = Post::where('id', $id)->with(['tags', 'categories', 'users'])->first();


        $data['allCategory'] = Category::leftjoin('posts', 'posts.category_id', 'categories.id')
            ->where('posts.post_status',1)
            ->select('categories.*', DB::raw('count(posts.id) as total'))
            ->groupBy('categories.id')->get();

        $data['posts'] = Post::where('post_status', 1)->inRandomOrder()->limit(4)->with(['tags', 'categories', 'users'])->get();
        //dd($blogDetails);
        return view('blog.blog_detail', $data);
    }
}
