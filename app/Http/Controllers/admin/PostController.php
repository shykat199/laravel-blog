<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        //$currentUser=Auth::user()->id;
        //dd($currentUser);
        if (Auth::user()->user_role==='admin'){
            $all_posts = Post::query()->where('post_status', '=', 1);
            if ($request->has('search')) {
                $all_posts = $all_posts->where('post_title', 'like', "%{$request->get('search')}%")
                    ->orWhere('post_description', 'like', "%{$request->get('search')}%");
            }
            $all_posts = $all_posts->paginate(5);

            return view('admin.blog.index', compact('all_posts'));
        }else{
            $all_posts = Post::query()->where('post_status', '=', 1)->where("user_id", '=', Auth::user()->id);
            if ($request->has('search')) {
                $all_posts = $all_posts->where('post_title', 'like', "%{$request->get('search')}%")
                    ->orWhere('post_description', 'like', "%{$request->get('search')}%");
            }
            $all_posts = $all_posts->paginate(5);

            return view('admin.blog.index', compact('all_posts'));
        }

    }

    public function inactive(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {

        if (Auth::user()->user_role==='admin'){
            $all_inactive_posts = Post::query()->where('post_status', '=', 0);
            if ($request->has('search')) {
                $all_inactive_posts = $all_inactive_posts->where('post_title', 'like', "%{$request->get('search')}%")
                    ->orWhere('post_description', 'like', "%{$request->get('search')}%");
            }
            $all_inactive_posts = $all_inactive_posts->paginate(5);

            return view('admin.blog.inactive', compact('all_inactive_posts'));
        }else{
            $all_inactive_posts = Post::query()->where('post_status', '=', 0)->where("user_id", '=', Auth::user()->id);
            if ($request->has('search')) {
                $all_inactive_posts = $all_inactive_posts->where('post_title', 'like', "%{$request->get('search')}%")
                    ->orWhere('post_description', 'like', "%{$request->get('search')}%");
            }
            $all_inactive_posts = $all_inactive_posts->paginate(5);

            return view('admin.blog.inactive', compact('all_inactive_posts'));
        }


    }

    public function blogActive(Request $request): \Illuminate\Http\RedirectResponse
    {
        $id = $request->get('blog_id');
        //dd($id);
        //$inactiveToActive = Post::find($id)->first();
        $inactiveToActive = Post::where('id', $id)->first();
        //dd($inactiveToActive);
        $inactiveToActive = $inactiveToActive->update([
            'post_status' => 1
        ]);


        if ($inactiveToActive) {
            return to_route('blog.index')->with('success', 'Post Is Now Active');
        } else {
            return Redirect::back()->with('error', 'Post Status Not Able To Change');
        }
    }

    public function blogInActive(Request $request): \Illuminate\Http\RedirectResponse
    {
        $id = $request->get('blog_id');
        $inactiveToActive = Post::where('id', $id)->first();
        $inactiveToActive = $inactiveToActive->update([
            'post_status' => 0
        ]);
        if ($inactiveToActive) {
            return to_route('blog.inactive')->with('success', 'Post Is Now Inactive');
        } else {
            return Redirect::back()->with('error', 'Post Status Not Able To Change');
        }

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $allCategories = Category::all();
        $allTags = Tag::all();
        return view('admin.blog.add', compact('allCategories', 'allTags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request): \Illuminate\Http\RedirectResponse
    {
        //dd($request->all());
        $user_id = $request->get('user_id');

        if ($request->file('post_image')) {

            $image = $request->file('post_image')->store('public/images');

//            $img = $request->get('post_image');
//            $customName = time() . rand(1, 99);
//            $ext = $request->get('post_image')->getClientOriginalExtension();
//            $path = '/image' . $customName . '.' . $ext;
//            $fileName = $customName . '.' . $ext;
//            $img->storeAs('image', $fileName, 'public');

            $store_post = Post::create([
                'user_id' => $user_id,
                'category_id' => $request->get('category_id'),
                'post_title' => $request->get('blog_title'),
                'post_description' => $request->get('post_description'),
                'post_image' => $image,
            ]);
            if ($request->has('tags')) {
                $store_post->tags()->attach($request->get('tags'));
            }
        }
        if ($store_post) {
            return to_route('blog.inactive')->with('success', 'Post Added Successfully');
        } else {
            return to_route('blog.create')->with('error', 'Post Not Added Successfully');

        }


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $post = Post::where('id', $id)->with('tags')->first();
        $category = Category::all();
        $tags = Tag::all();
        return view('admin.blog.show', compact('post', 'category', 'tags'));
    }

    public function showInactive(string $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $post = Post::where('id', $id)->with('tags')->first();
        $category = Category::all();
        $tags = Tag::all();
        return view('admin.blog.inactiveShow', compact('post', 'category', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $post = Post::where('id', $id)->with('tags')->first();
        $allCategories = Category::all();
        $allTags = Tag::all();
        return view('admin.blog.edit', compact('post', 'allCategories', 'allTags'));
    }

    public function editInactive(string $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $post = Post::where('id', $id)->with('tags')->first();
        $allCategories = Category::all();
        $allTags = Tag::all();
        return view('admin.blog.editInactive', compact('post', 'allCategories', 'allTags'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): \Illuminate\Http\RedirectResponse
    {
//        $post = Post::where('id', $id)->where('post_status', 1)->first();
        $post = Post::where(['id' => $id, 'post_status' => 1])->first();
        $image = $post->post_image;
        if ($request->hasFile('post_image')) {
            !is_null($post->post_image) && Storage::delete($post->post_image);
            $image = $request->file('post_image')->store('public/images');
        }
        $up_post = $post->update([
            'category_id' => $request->get('category_id'),
            'post_title' => $request->get('blog_title'),
            'post_description' => $request->get('post_description'),
            'post_image' => $image,
        ]);

        if ($request->has('tags')) {
            $post->tags()->detach();
            $post->tags()->attach($request->get('tags'));
        }


        if ($up_post) {
            return to_route('blog.index')->with('success', 'Post Updated Successfully');
        } else {
            return to_route('blog.index')->with('error', 'Post Not Updated Successfully');

        }

    }

    public function updateInactive(Request $request, string $id): \Illuminate\Http\RedirectResponse
    {
        $post = Post::find($id)->where('post_status', 0)->first();
        $image = $post->post_image;

        if ($request->hasFile('post_image')) {
            !is_null($post->post_image) && Storage::delete($post->post_image);
            $image = $request->file('post_image')->store('public/images');
        }
        $up_post = $post->update([
            'category_id' => $request->get('category_id'),
            'post_title' => $request->get('blog_title'),
            'post_description' => $request->get('post_description'),
            'post_image' => $image,
        ]);
        if ($request->has('tags')) {
            $post->tags()->detach();
            $post->tags()->attach($request->get('tags'));
        }


        if ($up_post) {
            return to_route('blog.index')->with('success', 'Post Updated Successfully');
        } else {
            return to_route('blog.index')->with('error', 'Post Not Updated Successfully');

        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = Post::find($id)->first();
        !is_null($post->post_image) && Storage::delete($post->post_image);
        $post->tags()->detach();
        $dlt_post = $post->delete();

        if ($dlt_post) {
            return to_route('blog.index')->with('success', 'Post Deleted Successfully');
        } else {
            return to_route('blog.index')->with('error', 'Post Not Deleted Successfully');

        }

    }
}
