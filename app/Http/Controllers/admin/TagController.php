<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $all_tags = Tag::query();
        if ($request->has('search')) {
            $all_tags = $all_tags->where('tag_name', 'like', "%{$request->get('search')}%");
        }
        $all_tags = $all_tags->paginate(5);
        return view('admin.tag.index', compact('all_tags'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.tag.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TagRequest $request): \Illuminate\Http\RedirectResponse
    {

        $store_tag = Tag::create([
            'tag_name' => $request->get('tag_name'),
        ]);
        if ($store_tag) {
            return to_route('tag.index')->with('success', 'Tag Added Successfully');
        } else {
            return Redirect::back()->with('error', 'Tag not added successfully');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request): \Illuminate\Http\RedirectResponse
    {
        $tag_id = $request->get('tag_id');
        $up_tag = Tag::findOrFail($tag_id)->update([
            'tag_name' => $request->get('tag_name'),
        ]);
        if ($up_tag) {
            return to_route('tag.index')->with('success', 'Tag Updated Successfully');
        } else {
            return Redirect::back()->with('error', 'Tag not updated successfully');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $dlt_tag=Tag::findOrFail($id)->delete();
        if ($dlt_tag){
            return to_route('tag.index')->with('success', 'Tag Deleted Successfully');
        } else {
            return Redirect::back()->with('error', 'Tag not deleted successfully');
        }
    }
}
