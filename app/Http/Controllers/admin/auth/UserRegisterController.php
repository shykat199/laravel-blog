<?php

namespace App\Http\Controllers\admin\auth;

use App\Http\Controllers\admin\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Models\admin\Category;
use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        if (Auth::check()) {
            return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');
        }
        return view('admin.auth.register');
    }

    public function userIndex(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
//        if (Auth::check()) {
//            return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');
//        }
        $users = User::query();
        if ($request->has('search')) {
            $users = $users->where('name', 'like', "%{$request->get('search')}%")
                ->orWhere('email', 'like', "%{$request->get('search')}%");
        }
        $users = $users->latest()->paginate(5);


        return view('admin.user.index', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RegisterUserRequest $request): \Illuminate\Http\RedirectResponse
    {
        //dd($request->all());
        $email = $request->get('email');
        $emailValidate = filter_var($email, FILTER_VALIDATE_EMAIL);
        //dd($emailValidate);
        if (!$emailValidate) {
            return Redirect::back()->withInput($request->only('email'))->with('errorEmail', 'Your Email Format Is Wrong');
        }

        $store_user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_role' => $request->get('user_role'),
            'password' => Hash::make($request->get('password')),
        ]);

        if ($store_user) {
            return to_route('admin.login')->with('success', 'User Register Successfully');
        } else {
            return Redirect::back()->with('error', 'User Not Able To Register');
        }

    }


    public function storeUser(RegisterUserRequest $request): \Illuminate\Http\RedirectResponse
    {
        //dd($request->all());
        $email = $request->get('email');
        $emailValidate = filter_var($email, FILTER_VALIDATE_EMAIL);
        //dd($emailValidate);
        if (!$emailValidate) {
            return Redirect::back()->withInput($request->only('email'))->with('errorEmail', 'Your Email Format Is Wrong');
        }

        $store_user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_role' => $request->get('user_role'),
            'password' => Hash::make($request->get('password')),
        ]);

        if ($store_user) {
            return to_route('user.index')->with('success', 'User Register Successfully');
        } else {
            return Redirect::back()->with('error', 'User Not Able To Register');
        }

    }

    public function show(string $id){
        //$user=User::find($id);
        $user = User::leftJoin('posts', 'posts.user_id', 'users.id')
            ->select('users.*', DB::raw('count(posts.id) as total'))
            ->where('users.id',$id)
            ->groupBy('users.id')->first();
        //dd($user);
//        $userPost=User::with('posts')->where('users.id',$id)->first();
//        dd($userPost);
        return view('admin.user.edit',compact('user'));
    }


    public function update(Request $request)
    {
        //dd($request->all());
        $uId=$request->get('uid');
        $user=User::where('id',$uId)->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_role' => $request->get('user_role'),
            'password' => Hash::make($request->get('password')),
        ]);

        if ($user) {
            return to_route('user.index')->with('success', 'User Updated Successfully');
        } else {
            return Redirect::back()->with('error', 'User Not Able To Updated');
        }

    }

    public function destroy(Request $request)
    {
        $uId=$request->get('uid');
        //dd($uId);
        $dltUser=User::findOrFail($uId)->delete();
        if ($dltUser){
            return to_route('user.index')->with('success','User Deleted Successfully');
        }else{
            return Redirect::back();
        }

    }
}
