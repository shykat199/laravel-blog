<?php

namespace App\Http\Controllers\admin\auth;

use App\Http\Controllers\admin\Controller;
use Illuminate\Http\Request;

class AdminBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.layout.master_blog');
    }

}
