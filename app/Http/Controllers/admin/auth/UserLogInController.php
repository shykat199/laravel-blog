<?php

namespace App\Http\Controllers\admin\auth;

use App\Http\Controllers\admin\Controller;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserLogInController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): string
    {
        if (Auth::user() && Auth::check()) {

            if (Auth::user()->user_role==='admin'){
                return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');

            }elseif (Auth::user()->user_role==='manager'){

                return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');
            }else{
                return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');
            }
            //return redirect()->route('admin.dashboard')->with('fail', 'Please Login First');
        }
        return view('admin.auth.login');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function login(LoginUserRequest $request): \Illuminate\Http\RedirectResponse
    {
        //dd($request->all());

        $check = $request->all();
        //dd($check);

        if (Auth::attempt([
            'email' => $check['email'],
            'password' => $check['password']
        ]))
        {
           // return to_route(Auth::user()->user_role . '.dashboard')->with('success', 'Login Successfully');
            //dd('xxxx');
            if (Auth::user()->user_role === 'blogger') {

                return to_route('admin.dashboard')->with('success', 'Login Successfully');

            } elseif (Auth::user()->user_role === 'manager') {

                return to_route('admin.dashboard')->with('success', 'Login Successfully');

            } else {
                return to_route('blog.index')->with('success', 'Login Successfully');
            }
        } else {
            return Redirect::back()->with('error', 'Invalid Email Or Password');
        }
    }
    /**
     * Display the specified resource.
     */
    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();
        return to_route('admin.login')->with('success', 'User Logout Successfully');
    }
}
