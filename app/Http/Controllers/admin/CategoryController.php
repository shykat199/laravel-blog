<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $all_category = Category::query();
        if ($request->has('search')) {
            $all_category = $all_category->where('category_name', 'like', "%{$request->get('search')}%");
        }
        $all_category = $all_category->paginate(10);
        return view('admin.category.index', compact('all_category'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.category.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'category_name' => ['required'],
        ], [
            'category_name.required' => 'Please Give A Category Name.'
        ]);

//        dd($request->all());
        $store_category = Category::create([
            'category_name' => $request->get('category_name'),
        ]);

        if ($store_category) {
            return to_route('category.index')->with('success', 'Category Added Successfully');
        } else {
            return Redirect::back()->with('error', 'Can Not Added');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $cat_id = $request->get('category_id');
        $update_category = Category::findOrFail($cat_id)->update([
            'category_name' => $request->get('category_name')
        ]);
        if ($update_category) {
            return to_route('category.index')->with('success', 'Category Updated Successfully');
        } else {
            return Redirect::back()->with('error', 'Category Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $dlt_category = Category::find($id);
        $dlt_category->posts()->delete();

        //$dlt_category = Category::findOrFail($id)->delete();
        //dd($dlt_category);
        if ($dlt_category) {
            DB::table('categories')->where('id',$id)->delete();
            return to_route('category.index')->with('success', 'Category Deleted Successfully');
        } else {
            return Redirect::back()->with('error', 'Category Can not deleted');
        }
    }
}
