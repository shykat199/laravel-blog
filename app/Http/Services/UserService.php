<?php

namespace App\Http\Services;

use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function userCreate(Request $request)
    {
        $store_user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_role' => $request->get('user_role'),
            'password' => Hash::make($request->get('password')),
        ]);

        return $store_user;
    }
}
