<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'blog_title' => ['required'],
            'category_id' => ['required'],
            'tags' => ['required'],
            'post_image' => ['required','max:2048'],
            'post_description' => ['required'],
        ];
    }

    public function messages(): array
    {
        return [
            'blog_title.required' => 'Please Give A BlogController Title',
            'category_id.required' => 'Please Select A Category',
            'tags.required' => 'Please Select Tags For The Post',
            'post_image.required' => 'Please Select An Image For The Post',
            'post_image.mimes' => 'Please Select An Image Of Format "jpeg,png,jpg,gif,svg"',
            'post_image.max' => 'Please Select An Image Less Then 2MB',
            'post_description.required' => 'Please Give A Description For The Post',
        ];
    }
}
