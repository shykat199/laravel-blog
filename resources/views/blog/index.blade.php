@extends('blog.layout.master')
@section('blog.content')
    <section class="hero">
        <div class="hero-container">
            <h1>
                Welcome to Carnotautomart Car Blog
                for all the Good Stuff about Cars
            </h1>
            <p>Every gist about cars to help maintain, buy and sell cars.</p>
            <div class="search-div">
                <form action="{{url('blog/index')}}" method="get">
                    <input class="hero-input" name="search" value="{{isset($_GET['search']) ?? ""}}" type="text" placeholder="Search blog....">
                    <i type="submit" class="fa fa-search"></i>
                </form>

            </div>
        </div>
    </section>

    <section class="product-section">
        <div class="product-left-content">
            @foreach($allPosts as $post)
                <div class="product">
                    <a href="{{route('blog.single.show',$post->id)}}">
                        <img src="{{\Illuminate\Support\Facades\Storage::url($post->post_image)}}" alt=""
                             style="height: 250px; width: 250px; object-fit: cover">
                    </a>
                    <div class="product-details">
                        <h3><a href="">{{$post->post_title}}</a></h3>
                        <div class="prod-date">
                            <p>{{\Carbon\Carbon::parse($post->created_at)->isoFormat('MMM Do YYYY')}}</p>
                            <p class="prod-maker">{{$post->categories->category_name}}</p>
                        </div>
                        <h4>{{\Illuminate\Support\Str::limit($post->post_description,203,'....')}}</h4>
                        <a href="{{route('blog.single.show',$post->id)}}" class="title">Read More</a>
                        <hr class="horizontal-line">
                    </div>

                </div>
            @endforeach


        </div>
        <div class="product-right-content">
            <div class="product-right-details">
                <div class="abc">
                    <h3>Blog<span class="">Categories</span></h3>
                    <hr class="product-right-details-hr-1st">
                    @foreach($allCategories as $category)
                        <a href="javascript:void(0)" class="product-right-details-div">
                            <p>{{$category->category_name}}</p>
                            <span>({{$category->total}})</span>
                        </a>
                    @endforeach


                    <hr class="product-right-details-hr-2nd">
                </div>
            </div>
            <div class="product-right-details">
                <h3>Recent<span class="">Post</span></h3>
                <hr class="recent-post-hr">
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="./assets/images/IMAGE (7).png" alt="">
                    </div>
                    <div>
                        <h5>Audi A4 '2016</h5>
                        <p>2.0 cm³</p>
                        <h5 class="design-h5">€45.000</h5>
                    </div>
                </a>
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="./assets/images/IMAGE (8).png" alt="">
                    </div>
                    <div>
                        <h5>Lamborghini Aventador '2012</h5>
                        <p>6.5 cm³, 30000 km</p>
                        <h5 class="design-h5">€150.000</h5>
                    </div>
                </a>
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="./assets/images/IMAGE (9).png" alt="">
                    </div>
                    <div>
                        <h5>Audi Quattro '2012</h5>
                        <p>3 cm³, 75000 km</p>
                        <h5 class="design-h5">€90.000</h5>
                    </div>
                </a>
                <hr class="recent-post-hr">
            </div>
            <div class="product-right-details">
                <h3>Featured<span class="">Post</span></h3>
                <div class="featured-details-div">
                    @foreach($posts as $post)
                        <a href="{{route('blog.single.show',$post->id)}}" class="featured-info">
                            <h4>{{\Illuminate\Support\Str::limit($post->post_title,30,'....')}}</h4>
                            <span>{{\Illuminate\Support\Str::limit($post->post_description,20,'....')}}</span>
                            <div class="featured-info-name">
                                <p>By {{$post->users->name}}</p>
                                <p>| {{\Carbon\Carbon::parse($post->created_at)->isoFormat('MMM Do YYYY')}}</p>
                            </div>
                        </a>
                    @endforeach


                </div>
            </div>
        </div>
    </section>

    <section class="pagination-section">
        <div class="pagination-section-1st-div"></div>
        <div>
            <ul class="pagination-section-num">
                <li>&lt;</li>
                <li class="love-icon-click">1</li>
                <li class="">2</li>
                <li class="">3</li>
                <li class="">4</li>
                <li class="">5</li>
                <li class="">6</li>
                <li class="">7</li>
                <li class="">8</li>
                <li class="">9</li>
                <li class="">10</li>
                <li>&gt;</li>
            </ul>
        </div>
    </section>

@endsection
