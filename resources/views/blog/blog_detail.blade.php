@extends('blog.layout.master')
@section('blog.content')

    <section class="navigator">
        <ul class="d-flex">
            <li><a href="javascript:void(0)" class="blog-base-color">Blog</a></li>
            <li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            </li>
            <li><a href="javascript:void(0)" class="blog-base-color">Cars</a></li>
            <li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            </li>
            <li><a href="javascript:void(0)"><span> Finbus Bonorum et Malorum</span></a></li>
        </ul>
    </section>
    <section class="blog-product-section">
        <div class="product-left-content">
            <div class="product-left-content-image">
                <img src="{{asset("frontend/assets/images/blogHero.jpg")}}" alt="blog hero">
            </div>
            <h2><a href="">{{$blogDetails->post_title}}</a></h2>
            <div class="d-flex writter justify-content-between">
                <ul class="d-flex writter-name">
                    <li><span class="blog-imp-color">Written</span> <span
                            class="blog-base-color">by {{$blogDetails->users->name}}</span>
                    </li>
                    <li>{{\Carbon\Carbon::parse($blogDetails->created_at)->isoFormat('MMM Do YYYY')}}</li>
                </ul>
                <ul class="d-flex writter-social-media">
                    <li>
                        <a href="">
                            <img src="{{asset("frontend/assets/images/facebook-icon.png")}}" alt=""></a>
                    </li>
                    <li>
                        <a href="">
                            <img src="{{asset("frontend/assets/images/twitter.png")}}" alt=""></a>
                    </li>
                </ul>
            </div>
            <div class="written-blog">
                <p>
                    {{$blogDetails->post_description}}
                </p>
            </div>
            <div class="d-flex justify-content-between tag-part">
                <ul class="d-flex tag-part-left">
                    <li><a href="">Tags</a></li>
                    @foreach($blogDetails->tags as $tag)
                        <li><a href="" class="blog-base-color tag-button">{{$tag->tag_name}}</a></li>
                    @endforeach

                </ul>
                <ul class="d-flex tag-part-right">
                    <li><a href="">Category: <span
                                class="blog-base-color tag-button"> {{$blogDetails->categories->category_name}}</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="product-right-content">
            <div class="product-right-details blog-product-right-details">
                <div class="abc">
                    <h3>Blog<span class="">Categories</span></h3>
                    <hr class="product-right-details-hr-1st">
                    @foreach($allCategory as $category)
                        <a href="javascript:void(0)" class="product-right-details-div">
                            <p>{{$category->category_name}}</p>
                            <p>({{$category->total}})</p>

                        </a>
                    @endforeach

                    <hr class="product-right-details-hr-2nd">
                </div>
            </div>
            <div class="product-right-details">
                <h3>Recent<span class="">Post</span></h3>
                <hr class="recent-post-hr">
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="{{asset("frontend/assets/images/IMAGE (7).png")}}" alt="">
                    </div>
                    <div>
                        <h5>Audi A4 '2016</h5>
                        <p>2.0 cm³</p>
                        <h5 class="design-h5">€45.000</h5>
                    </div>
                </a>
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="{{asset("frontend/assets/images/IMAGE (8).png")}}" alt="">
                    </div>
                    <div>
                        <h5>Lamborghini Aventador '2012</h5>
                        <p>6.5 cm³, 30000 km</p>
                        <h5 class="design-h5">€150.000</h5>
                    </div>
                </a>
                <a href="javascript:void(0)" class="recent-post-div">
                    <div>
                        <img src="{{asset("frontend/assets/images/IMAGE (9).png")}}" alt="">
                    </div>
                    <div>
                        <h5>Audi Quattro '2012</h5>
                        <p>3 cm³, 75000 km</p>
                        <h5 class="design-h5">€90.000</h5>
                    </div>
                </a>
                <hr class="recent-post-hr">
            </div>
            <div class="product-right-details">
                <h3>Featured<span class="">Post</span></h3>
                <div class="featured-details-div">
                    @foreach($posts as $post)
                        <a href="{{route('blog.single.show',$post->id)}}" class="featured-info">
                            <h4>{{\Illuminate\Support\Str::limit($post->post_title,30,'....')}}</h4>
                            <span>{{\Illuminate\Support\Str::limit($post->post_description,20,'....')}}</span>
                            <div class="featured-info-name">
                                <p>By {{$post->users->name}}</p>
                                <p>| {{\Carbon\Carbon::parse($post->created_at)->isoFormat('MMM Do YYYY')}}</p>
                            </div>
                        </a>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <section class="related-blog-section">
        <h3><span class="blog-base-color">Related </span>Blog Post</h3>
        <ul>
            @foreach($posts as $post)
                <li>
                    <a href="{{route("blog.single.show",$post->id)}}" class="d-flex blog-content">
                        <img src="{{asset("frontend/assets/images/blog-car.jpg")}}" alt="car picture">
                        <div class="blog-details">
                            <h3>{{\Illuminate\Support\Str::limit($post->post_title,30,'....')}}</h3>
                            <p class="blog-details-text">{{\Illuminate\Support\Str::limit($post->post_description,100,'....')}}</p>
                            <span class="d-flex blog-base-color blogger-name">
                            <p>By {{$post->users->name}}</p>
                            <span>|</span>
                            <p> {{\Carbon\Carbon::parse($post->created_at)->isoFormat('MMM Do YYYY')}}</p>
                        </span>
                        </div>
                    </a>
                </li>
            @endforeach


        </ul>
    </section>

@endsection
