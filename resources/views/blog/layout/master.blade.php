<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Carnotautomart</title>
    <link rel="stylesheet" href="{{asset("frontend/assets/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("frontend/assets/css/default.css")}}">
    <link rel="stylesheet" href="{{asset("frontend/assets/css/style.css")}}">

</head>

<body>
<header class="header-area">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <div class="header-logo-menu d-flex align-items-center">
                <a href="{{url('blog/index')}}" class="logo">
                    <img src="{{asset("frontend/assets/images/image 3.png")}}" alt="logo"/>
                </a>
                <ul class="mainmenu d-flex align-items-center">
                    <li>
                        <a href="Javascript:void(0)">Buy <i class="fa fa-angle-down"></i></a>
                        <ul class="submenu">
                            <li><a href="Javascript:void(0)">submenu1</a></li>
                            <li>
                                <a href="Javascript:void(0)">submenu2</a>
                                <ul class="submenu">
                                    <li><a href="Javascript:void(0)">submenu1</a></li>
                                    <li><a href="Javascript:void(0)">submenu2</a></li>
                                    <li><a href="Javascript:void(0)">submenu3</a></li>
                                    <li><a href="Javascript:void(0)">submenu4</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="Javascript:void(0)">submenu3</a>
                                <ul class="submenu">
                                    <li><a href="Javascript:void(0)">submenu1</a></li>
                                    <li><a href="Javascript:void(0)">submenu2</a></li>
                                    <li><a href="Javascript:void(0)">submenu3</a></li>
                                    <li><a href="Javascript:void(0)">submenu4</a></li>
                                </ul>
                            </li>
                            <li><a href="Javascript:void(0)">submenu4</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Sell <i class="fa fa-angle-down"></i></a>
                        <ul class="submenu">
                            <li><a href="Javascript:void(0)">submenu1</a></li>
                            <li>
                                <a href="Javascript:void(0)">submenu2</a>
                                <ul class="submenu">
                                    <li><a href="Javascript:void(0)">submenu1</a></li>
                                    <li><a href="Javascript:void(0)">submenu2</a></li>
                                    <li><a href="Javascript:void(0)">submenu3</a></li>
                                    <li><a href="Javascript:void(0)">submenu4</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="Javascript:void(0)">submenu3</a>
                                <ul class="submenu">
                                    <li><a href="Javascript:void(0)">submenu1</a></li>
                                    <li><a href="Javascript:void(0)">submenu2</a></li>
                                    <li><a href="Javascript:void(0)">submenu3</a></li>
                                    <li><a href="Javascript:void(0)">submenu4</a></li>
                                </ul>
                            </li>
                            <li><a href="Javascript:void(0)">submenu4</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Hire <i class="fa fa-angle-down"></i></a>
                    </li>
                </ul>
            </div>
            <ul class="header-right d-flex align-items-center">
                <li><i class="fa fa-heart love-icon"></i></li>
                <li><a class="login" href="#">Log in</a></li>
                <li><a class="signup" href="#">Sign up</a></li>
            </ul>
        </div>
    </div>
</header>

@yield('blog.content')

<footer>
    <section class="footer-section">
        <div class="footer-container">
            <h2>
                Retain the old footer
            </h2>
            <p>Remove the Google playstore and app store icon from the footer</p>
            <p>Use this background colour</p>
        </div>
    </section>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(".love-icon").click(function () {
        $(".love-icon").toggleClass("love-icon-click");
    });
    $(".pagination-section-num li").on("click", function () {
        $(".pagination-section-num li").removeClass("love-icon-click");
        $(this).addClass("love-icon-click");
    });

</script>
</body>

</html>
