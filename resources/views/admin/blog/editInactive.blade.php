@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Update Active Blog</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Update Blog</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('error')}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif


    <div class="card">
        <div class="card-header">
            Update Active Blog
            @if(\Illuminate\Support\Facades\Auth::user()->user_role === 'admin')
                <div class="float-right">
                    <form action="{{route('blog.inactive.active')}}" method="post">
                        @csrf
                        <input type="hidden" name="blog_id" value="{{$post->id}}" id="">
                        @csrf
                        @if($post->post_status === 0)
                            <button type="submit" class="btn btn-primary">Active This Post</button>

                        @endif
                    </form>
                </div>
            @endif

        </div>
        <div class="card-body">

            <form action="{{route('blog.update',$post->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" name="user_id" id="" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">

                <div class="form-group">
                    <label for="blog_title">Blog Title</label>
                    <input type="text" name="blog_title" value="{{$post->post_title}}" class="form-control"
                           id="blog_title" placeholder="Blog Title">
                </div>

                @error('blog_title')
                <span class="text-danger">{{$message}}</span>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Category Name</label>
                    <select class="form-control" name="category_id" id="exampleFormControlSelect1">
                        <option selected value="">Select Category</option>
                        @foreach($allCategories as $category)
                            <option
                                value="{{$category->id}}" {{$category->id === $post->category_id ? 'selected': ""}}>{{$category->category_name}}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect2">Select Tag</label>
                    <select class="form-control js-example-tags" name="tags[]" multiple="multiple">

                        @foreach($allTags as $tag)
                            <option value="{{$tag->id}}">{{$tag->tag_name}}</option>
                        @endforeach

                    </select>
                    @error('tags')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Post Description</label>
                    <textarea name="post_description" class="form-control" id="exampleFormControlTextarea1" rows="3">
                        {{$post->post_description}}
                    </textarea>
                    @error('post_description')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="post_image">Blog Image</label>
                    <input type="file" name="post_image" class="form-control" id="post_image"
                           onchange="readUrl1(this)">
                    <img class="mt-2" id="image1" src="{{\Illuminate\Support\Facades\Storage::url($post->post_image)}}"
                         alt="" style="height: 150px;width: 150px"/>
                    @error('post_image')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>


                <button type="submit" class="btn btn-primary">Update</button>


            </form>


        </div>
    </div>

    <script>
        function readUrl1(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('#image1')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(".js-example-tags").select2({
            tags: true,
            tokenSeparators: [',', ' '],
        });
    </script>
@endsection
