@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">All Inactive Posts</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">New Post</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('error')}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="card mt-2">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <strong> All Inactive Posts </strong>
                </div>
                <div class="col">
                    <form action="{{route('blog.inactive')}}" method="get">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" value="{{isset($_GET['search']) ?? ""}}" id="form1" name="search" class="form-control"
                                       placeholder="Search...."/>
                            </div>
                            <button type="submit" class="btn btn-primary ml-1">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <div class="float-right">
                        <a href="{{route('blog.create')}}" class="btn btn-success">
                            Add
                            New Post
                        </a>
                    </div>
                </div>
            </div>

        </div>
        @if(\Illuminate\Support\Facades\Auth::user()->user_role==='admin')
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Post Title</th>
                        <th scope="col">Author Name</th>
                        <th scope="col">Post Description</th>
                        <th scope="col">Post Image</th>
                        <th scope="col">Post Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $idx=1;
                    @endphp
                    @foreach($all_inactive_posts as $post)
                        <tr>
                            <th>{{$idx++}}</th>
                            <td>{{\Illuminate\Support\Str::limit($post->post_title,20,'....')}}</td>
                            <td>{{$post->users->name}}</td>
                            <td>{{\Illuminate\Support\Str::limit($post->post_description,60,'....')}}</td>
                            <td>
                                <img src="{{\Illuminate\Support\Facades\Storage::url($post->post_image)}}" alt=""
                                     style="height: 80px;width: 80px">
                            </td>
                            <td>

                                @if($post->post_status===0)
                                    <span class="badge badge-danger">InActive</span>
                                @else
                                    <span class="badge badge-success">Active</span>
                                @endif
                            </td>
                            <td>
                                <div style="display: flex">
                                    <a href="{{route('blog.inactive.show',$post->id)}}"
                                       class="btn btn-primary mr-2 text-white btnEdit">Show</a>

                                    <a href="{{route('blog.inactive.edit',$post->id)}}"
                                       class="btn btn-warning mr-2 text-white btnEdit">Edit</a>

                                    <a href="{{route('blog.delete',$post->id)}}" class="btn btn-danger mr-2 text-white">Delete</a>

                                </div>

                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if ($all_inactive_posts->hasPages())
                    <div class="pagination-wrapper">
                        {{ $all_inactive_posts->links() }}
                    </div>
                @endif
            </div>

        @else
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Post Title</th>
                        <th scope="col">Post Description</th>
                        <th scope="col">Post Image</th>
                        <th scope="col">Post Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $idx=1;
                    @endphp
                    @foreach($all_inactive_posts as $post)
                        <tr>
                            <th>{{$idx++}}</th>
                            <td>{{\Illuminate\Support\Str::limit($post->post_title,20,'....')}}</td>
                            <td>
                            <textarea readonly name="post_description" class="form-control"
                                      id="exampleFormControlTextarea1" rows="3">
                                {{\Illuminate\Support\Str::limit($post->post_description,20,'....')}}
                            </textarea>
                            </td>
                            <td>
                                <img src="{{\Illuminate\Support\Facades\Storage::url($post->post_image)}}" alt=""
                                     style="height: 80px;width: 80px">
                            </td>
                            <td>

                                @if($post->post_status===0)
                                    <span class="badge badge-danger">InActive</span>
                                @else
                                    <span class="badge badge-success">Active</span>
                                @endif
                            </td>
                            <td>
                                <div style="display: flex">
                                    <a href="{{route('blog.inactive.show',$post->id)}}"
                                       class="btn btn-primary mr-2 text-white btnEdit">Show</a>

                                    <a href="{{route('blog.inactive.edit',$post->id)}}"
                                       class="btn btn-warning mr-2 text-white btnEdit">Edit</a>

                                    <a href="{{route('blog.delete',$post->id)}}" class="btn btn-danger mr-2 text-white">Delete</a>

                                </div>

                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if ($all_inactive_posts->hasPages())
                    <div class="pagination-wrapper">
                        {{ $all_inactive_posts->links() }}
                    </div>
                @endif
            </div>
        @endif

    </div>

@endsection
