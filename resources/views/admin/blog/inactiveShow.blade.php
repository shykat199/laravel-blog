@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Show Blog</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Show Blog</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="card">
        <div class="card-header">
            Show Blog
        </div>
        <div class="card-body">

            <div class="form-group">
                <label for="blog_title">Blog Title</label>
                <input readonly type="text" value="{{$post->post_title}}" name="blog_title" class="form-control" id="blog_title" placeholder="Blog Title">
            </div>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Category Name</label>
                <input readonly type="text" value="{{$post->categories->category_name}}" name="blog_title" class="form-control" id="blog_title" placeholder="Blog Title">


            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect2">Selected Tag</label>
                <ul>
                    @foreach($post->tags as $tag)
                        <li>
                            {{$tag->tag_name}}
                        </li>
                    @endforeach
                </ul>



            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Post Description</label>
                <textarea readonly name="post_description" class="form-control" id="exampleFormControlTextarea1" rows="3">
                    {{$post->post_description}}
                </textarea>

            </div>
            <div class="form-group">
                <label for="post_image">Blog Image</label>
                <br>
                <img class="mt-2" id="image1" src="{{\Illuminate\Support\Facades\Storage::url($post->post_image)}}" alt="" style="height: 100px; width: 100px"/>

            </div>
        </div>
    </div>

@endsection
