@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Add New User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add New User</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('error')}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="card mt-2">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <strong> All Users </strong>
                </div>
                <div class="col">
                    <form action="{{(route('user.index'))}}" method="get">

                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" value="{{isset($_GET['search']) ?? ""}}" name="search" class="form-control"
                                       placeholder="Search...."/>
                            </div>
                            <button type="submit" class="btn btn-primary ml-1">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <div class="float-right">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                            Add
                            New User
                        </button>
                    </div>
                </div>
            </div>


        </div>

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#ID</th>
                    <th scope="col">User Name</th>
                    <th scope="col">User Email</th>
                    <th scope="col">User Role</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $idx=1;
                @endphp
                @foreach($users as $user)
                    <tr>
                        <th>{{$idx++}}</th>
                        <td class="user_name" data-id="{{$user->id}}">{{$user->name}}</td>
                        <td class="user_email" >{{$user->email}}</td>
                        <td class="user_role" >{{$user->user_role}}</td>
                        <td>
                            <div style="display: flex">
                                <button type="button" class="btn btn-warning mr-2 text-white btnEdit"
                                        data-toggle="modal"
                                        data-target="#exampleModal1">Edit
                                </button>

                                <a href="{{route('user.show',$user->id)}}" class="btn btn-primary mr-2 text-white">Show</a>

                                <form action="{{route('user.delete')}}"

                                      method="post"
                                      onsubmit="return confirm('Are You Sure?');"
                                >
                                    @csrf
                                    <input type="hidden" name="uid" value="{{$user->id}}">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                                {{--                                <a href="{{route('tag.delete',$user->id)}}"--}}
                                {{--                                   class="btn btn-danger mr-2 text-white">Delete</a>--}}

                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

            @if ($users->hasPages())
                <div class="pagination-wrapper">
                    {{ $users->links() }}
                </div>
            @endif

        </div>
    </div>

    <!--Add Modal 1 -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.store')}}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div>
                            <label for="inputPassword5" class="form-label">User Name</label>
                            <input type="text" name="name" id="inputPassword5" class="form-control"
                                   placeholder="User Name"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">User Email</label>
                            <input type="email" name="email" id="inputPassword5" class="form-control"
                                   placeholder="User Email"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @if(\Illuminate\Support\Facades\Session::has('errorEmail'))
                            <span class="text-danger">
                                {{\Illuminate\Support\Facades\Session::get('errorEmail')}}
                            </span>
                        @endif
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">User Password</label>
                            <input type="password" name="password" id="inputPassword5" class="form-control"
                                   placeholder="User Password"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('password')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="inputPassword5" class="form-control"
                                   placeholder="Confirm Password"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('password_confirmation')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                        <div>
                            <label for="inputPassword5" class="form-label">User Role</label>
                            <select name="user_role" class="form-control" aria-label="Default select example">
                                <option selected>Select The User Role</option>
                                <option value="admin">Admin</option>
                                <option value="manager">Manager</option>
                                <option value="blogger">Blogger</option>
                            </select>
                        </div>
                        @error('user_role')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--Edit Modal 2 -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.update')}}" method="post">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="uid" id="uid">
                        <div>
                            <label for="inputPassword5" class="form-label">User Name</label>
                            <input type="text" name="name" id="user_name" class="form-control"
                                   placeholder="User Name"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">User Email</label>
                            <input type="email" name="email" id="user_email" class="form-control"
                                   placeholder="User Email"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @if(\Illuminate\Support\Facades\Session::has('errorEmail'))
                            <span class="text-danger">
                                {{\Illuminate\Support\Facades\Session::get('errorEmail')}}
                            </span>
                        @endif
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">User Password</label>
                            <input type="password" name="password" id="user_password" class="form-control"
                                   placeholder="User Password"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('password')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div>
                            <label for="inputPassword5" class="form-label">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="inputPassword5" class="form-control"
                                   placeholder="Confirm Password"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('password_confirmation')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                        <div>
                            <label for="inputPassword5" class="form-label">User Role</label>
                            <select name="user_role" id="user_role" class="form-control" aria-label="Default select example">
                                <option selected>Select The User Role</option>
                                <option value="admin">Admin</option>
                                <option value="manager">Manager</option>
                                <option value="blogger">Blogger</option>
                            </select>
                        </div>
                        @error('user_role')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
            integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>

        $(document).ready(function () {
            $('.btnEdit').on('click', function () {
                let currentRow = $(this).closest('tr');
                let userName = currentRow.find('.user_name').html();
                let userEmail = currentRow.find('.user_email').html();
                let userRole = currentRow.find('.user_role').html();
                let userId = currentRow.find(".user_name").data('id');

                $("#uid").val(userId);
                $("#user_name").val(userName);
                $("#user_email").val(userEmail);
                $("#user_role").val(userRole);
            })
        })

    </script>

@endsection
