@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Show User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Show User</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('error')}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="card mt-2">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <strong> Show User </strong>
                </div>

            </div>


        </div>

        <div class="card-body">
            <form action="{{route('user.update')}}" method="post">
                <div class="modal-body">
                    @csrf

                    <div class="row">
                        <div class="col">
                            <div>
                                <label for="inputPassword5" class="form-label">User Name</label>
                                <input readonly type="text" value="{{$user->name}}" name="name" id="user_name"
                                       class="form-control"
                                       placeholder="User Name"
                                       aria-describedby="passwordHelpBlock">
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <label for="inputPassword5" class="form-label">User Email</label>
                                <input readonly type="email" value="{{$user->email}}" name="email" id="user_email"
                                       class="form-control"
                                       placeholder="User Email"
                                       aria-describedby="passwordHelpBlock">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">

                            <label for="inputPassword5" class="form-label">Number Of Blog</label>
                            <input readonly type="text" value="{{$user->total}}" id="user_password" class="form-control"
                                   placeholder="User Password"
                                   aria-describedby="passwordHelpBlock">

                        </div>
                        <div class="col">
                            <div class="col">
                                <label for="inputPassword5" class="form-label">User Role</label>
                                <input readonly type="text" value="{{$user->user_role}}" id="user_password"
                                       class="form-control"
                                       placeholder="User Password"
                                       aria-describedby="passwordHelpBlock">
                            </div>
                        </div>

                    </div>
                    <br>

                    <label for="inputPassword5" class="form-label">User Post List</label>
                    <div class="row">

                        <div class="card">
                            <div class="card-body">
                                @if (count($user->posts))
                                    <ul>
                                        @foreach($user->posts as $post)
                                            <li>{{$post->post_title}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    No Post Found
                                @endif

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="uid" id="uid">
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>





    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
            integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
