@extends('admin.auth.layout.master_auth')

@section('admin.auth')

    <p class="login-box-msg">Register a new membership</p>

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-danger">
            {{\Illuminate\Support\Facades\Session::get('error')}}
        </div>
    @endif

    <form action="{{route('admin.store.new')}}" method="post">
        @csrf
        @method('POST')
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="name" placeholder="Full name" value="{{old('name') ?? ""}}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
            </div>
        </div>
        @error('name')
        <span class="text-danger">{{$message}}</span>
        @enderror
        <div class="input-group mb-3">
            <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')?? ""}}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
        </div>
        @if(\Illuminate\Support\Facades\Session::has('errorEmail'))
            <span class="text-danger">
                {{\Illuminate\Support\Facades\Session::get('errorEmail')}}
            </span>
        @endif
        @error('email')
        <span class="text-danger">{{$message}}</span>
        @enderror

        <div class="input-group mb-3">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        @error('password')
        <span class="text-danger">{{$message}}</span>
        @enderror
        <div class="input-group mb-3">
            <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
            @error('password_confirmation')
            <span class="text-danger">{{$message}}</span>
            @enderror

        </div>
        <div class="row">
            <div class="col-8">
                <div class="icheck-primary">
                    <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                    <label for="agreeTerms">
                        I agree to the <a href="#">terms</a>
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
            <i class="fab fa-facebook mr-2"></i>
            Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
            <i class="fab fa-google-plus mr-2"></i>
            Sign up using Google+
        </a>
    </div>

    <a href="login.html" class="text-center">I already have a membership</a>

@endsection
