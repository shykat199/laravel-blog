@extends('admin.layout.master_blog')

@section('admin.blog.content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Add New Tag</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add New Tag</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{\Illuminate\Support\Facades\Session::get('error')}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="card mt-2">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <strong> All Tags </strong>
                </div>
                <div class="col">
                    <form action="{{(route('tag.index'))}}" method="get">

                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" value="{{isset($_GET['search']) ?? ""}}" id="form1" name="search" class="form-control"
                                       placeholder="Search...."/>
                            </div>
                            <button type="submit" class="btn btn-primary ml-1">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <div class="float-right">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                            Add
                            New Tag
                        </button>
                    </div>
                </div>
            </div>


        </div>

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#ID</th>
                    <th scope="col">Tag Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $idx=1;
                @endphp
                @foreach($all_tags as $tag)
                    <tr>
                        <th>{{$idx++}}</th>
                        <td class="tag_name" data-id="{{$tag->id}}">{{$tag->tag_name}}</td>
                        <td>
                            <div style="display: flex">
                                <button type="button" class="btn btn-warning mr-2 text-white btnEdit"
                                        data-toggle="modal"
                                        data-target="#exampleModal1">Edit
                                </button>

                                {{--                                <form action="{{route('category.delete',$category->id)}}"--}}
                                {{--                                      method="post"--}}
                                {{--                                      onsubmit="return confirm('Are You Sure?');"--}}
                                {{--                                >--}}
                                {{--                                    <button class="btn btn-danger" type="submit">Delete</button>--}}
                                {{--                                </form>--}}
                                <a href="{{route('tag.delete',$tag->id)}}"
                                   class="btn btn-danger mr-2 text-white">Delete</a>

                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            @if ($all_tags->hasPages())
                <div class="pagination-wrapper">
                    {{ $all_tags->links() }}
                </div>
            @endif
        </div>
    </div>

    <!--Add Modal 1 -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('tag.store')}}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div>
                            <label for="inputPassword5" class="form-label">Tag Name</label>
                            <input type="text" name="tag_name" id="inputPassword5" class="form-control"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('tag_name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--Edit Modal 2 -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('tag.update')}}" method="post">
                    @csrf

                    <input type="hidden" id="tag_id" name="tag_id">

                    <div class="modal-body">
                        <div>
                            <label for="inputPassword5" class="form-label">Tag Name</label>
                            <input type="text" name="tag_name" id="tag_name" class="form-control"
                                   aria-describedby="passwordHelpBlock">
                        </div>
                        @error('tag_name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
            integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>

        $(document).ready(function () {
            $('.btnEdit').on('click', function () {
                let currentRow = $(this).closest('tr');
                let col1 = currentRow.find('.tag_name').html();
                let tag_id = currentRow.find(".tag_name").data('id');

                $("#tag_id").val(tag_id);
                $("#tag_name").val(col1);
            })
        })

    </script>

@endsection
